try
  hubot = require 'hubot'
catch
  prequire = require 'parent-require'
  hubot = prequire 'hubot'

Adapter = hubot.Adapter
TextMessage = hubot.TextMessage
EnterMessage = hubot.EnterMessage
pusherclient = require 'pusher-client'
request = require 'request'
Path = require 'path'
FS = require 'fs'
Q = require 'q'

APP_KEY  = process.env.QB_APP_KEY
BOT_EMAIL = process.env.QB_BOT_EMAIL
BOT_PASSWD = process.env.QB_BOT_PASSWD
BOT_SERVER = process.env.QB_SERVER
BOT_TOKEN = process.env.QB_BOT_TOKEN


class Qiscus extends Adapter
  constructor: (@robot) ->
    super @robot
    @robot.logger.info "Constructor"
    @token = BOT_TOKEN || null

  send: (envelope, strings...) ->
    @robot.logger.debug "Send message for #{envelope.user.name} to #{envelope.user.room}"
    for str in strings
      request.post "#{BOT_SERVER}/api/v1/mobile/postcomment",
        'form':
          'token': BOT_TOKEN || @robot.brain.get 'robot:token'
          'comment': "#{str}"
          'topic_id': "#{envelope.user.room}"
        (err, resp, body) =>
          @robot.logger.debug "Response on sending message => #{body}"

  reply: (envelope, strings...) ->
    @robot.logger.debug "Reply message for #{envelope.user.name} to #{envelope.user.room}"
    strings = strings.map (str) -> "@#{envelope.user.name} #{str}"
    @send envelope, strings...

  run: ->
    @robot.logger.info 'Run'
    unless APP_KEY and BOT_EMAIL and BOT_PASSWD and BOT_SERVER
      @robot.logger.error "Need environment variabel QB_APP_KEY, QB_BOT_EMAIL, QB_BOT_PASSWD, QB_SERVER"
      process.exit 1
      return

    # Create a new pusher client
    @pusher = new pusherclient APP_KEY, {
      encrypted: true
    }

    # List of subscribe rooms
    @rooms = []

    Q.fcall () =>
        # Log in the robot first
        @robot.logger.info "Logging in"
        @.login(BOT_EMAIL, BOT_PASSWD)

      .then (resp) =>
        # After getting the token get the rooms that robot belong to
        token = resp.token
        name  = resp.name
        @.robot.logger.debug "Token= #{token}"

        @.robot.logger.info "Set robot name to #{name}"
        @.robot.name = name

        @.robot.logger.info "Save token to brain"
        @.robot.brain.set 'robot:token', token

        @.robot.logger.info "Getting room list"
        @.get_rooms token

      .then (rooms) =>
        @.robot.brain.set 'robot:rooms', rooms
        rooms

      .then (rooms) =>
        # Subscribe to pusher events, the `rooms` parameter only to pass it to next promise
        @.robot.logger.info 'Subscribe to pusher events'
        @.bot_real = @.pusher.subscribe BOT_EMAIL
        @.bot_real.bind 'newRoom', @.onNewRoom
        @.bot_real.bind 'deleteRoom', @.onDeleteRoom
        rooms

      .then (rooms) =>
        # Then subscribe to each rooms
        @.robot.logger.info "Subscribe to each room"
        @.subs_rooms rooms
        rooms

      .then (rooms) =>
        # Then save rooms list to brain so easier for later usage
        @.robot.logger.info "Saving room list to brain"
        @.rooms = rooms
        @.saveRooms()

      .then () =>
        # Emit connected event to hubot core
        @.robot.logger.info "Connected and ready"
        @.emit 'connected'

      .then () =>
        # Parse more-help command usage (qiscus-custom)
        @.robot.logger.info "Parse more help"
        @.parseMoreHelp()

      .catch (err) =>
        # Catch any error within the promise chains
        @.robot.logger.error err
        process.exit 1

      .done()   # And DONE!

  parseMoreHelp: () =>
    # Process documentation scripts for more command here?
    basedir = Path.resolve Path.dirname()
    scripts_dir = Path.join basedir, 'scripts'
    files = FS.readdirSync scripts_dir
    @moreCommands = []
    for file in files
      file = Path.join scripts_dir, file
      @robot.logger.debug "Parsing more help for #{file}"
      scriptName = Path.basename(Path.join scripts_dir, file).replace /\.(coffee|js)$/, ''
      scriptDocumentation = {}
      body = FS.readFileSync file, 'utf-8'

      currentSection = null
      for line in body.split '\n'
        break unless line[0] is '#' or line.substr(0, 2) is '//'

        cleanedLine = line.replace(/^(#|\/\/\s?)/, '').trim()

        continue if cleanedLine.length is 0
        continue if cleanedLine.toLowerCase() is 'none'

        __commands = ['more-help', 'description', 'dependencies',
                      'configuration', 'commands', 'author',
                      'examples', 'tags', 'urls']
        nextSection = cleanedLine.toLowerCase().replace(':', '')

        if nextSection in __commands
          currentSection = nextSection
          scriptDocumentation[currentSection] = []
        else
          if currentSection
            scriptDocumentation[currentSection].push cleanedLine.trim()
            if currentSection is 'more-help'
              cleanedLine = cleanedLine.replace(/hubot/i, @robot.name).trim()
              @moreCommands.push cleanedLine
    @robot.logger.info 'set robot:misc:morecommands to brain'
    @robot.brain.set 'robot:misc:morecommands', JSON.stringify {morecommands: @moreCommands}
    @robot.moreHelp = @moreCommands

  saveRooms: () =>
    # Only save room code and id in hubot brain, instead of the whole
    # json object
    try
      rooms = @.rooms.map (room) ->
        code = if room.code_encrypted? then room.code_encrypted else room.code_en
        {id: room.id, code_encrypted: code}
      @robot.brain.set 'robot:rooms', rooms
      @robot.brain.save()
    catch e
      @.catchError(e, 'Saving room')

  catchError: (e, _when) =>
    @robot.logger.error "Error when #{_when}\nError type=#{e.name}\nError message=#{e.message}"

  get_rooms: (token) =>
    Q.promise (resolve, reject, notify) =>
      __server = "#{BOT_SERVER}/api/v1/mobile/room_quin"
      @.robot.http(__server)
        .query({token: token})
        .get() (err, resp, body) ->
          if err
            reject {error: "Getting error while get room list", err: err}
          else
            try
              _body = JSON.parse body
              rooms = _body.results
            catch e
              rooms = undefined
            finally
              if rooms is undefined
                reject {rooms: rooms, error: "Getting error while get room list #{body}"}
              else
                resolve rooms

  login: (email, password) =>
    data =
      'user[email]': email
      'user[password]': password

    Q.promise (resolve, reject, notify) =>
      @.robot.http("#{BOT_SERVER}/users/sign_in.json")
        .query(data)
        .post() (err, res, body) ->
          if err
            reject {error: "Error in signin", err: err}

          try
            _body       = JSON.parse body
            token       = _body.token
            robot_name  = _body.username
          catch e
            token = undefined
          finally
            if token is undefined
              reject  {token: undefined, error: "Getting error while signing in #{body}"}
            else
              resolve {name: robot_name, token: token}

  subs_rooms: (rooms) =>
    rooms_code = rooms.map (r) -> r.code_encrypted
    rooms_code.forEach (code) =>
      sub = @.pusher.subscribe code
      sub.bind 'postcomment', @.onPostComment
    rooms

  onNewRoom: (data) =>
    # Get the rooms that robot must be subscribed
    # Add this room to subscribe list so robot will listen to this room

    # log the room data
    @robot.logger.debug "onNewRoom, #{JSON.stringify data}"

    # Handle subscription
    sub = @pusher.subscribe data.code_en
    room = {
      id: data.id
      code_encrypted: data.code_en
    }
    @rooms.push room

    sub.bind 'postcomment', @.onPostComment

    # Save room list to hubot brain
    @.saveRooms()
    user = @robot.brain.userForId @robot.name,
      room: data.listtopics[0].id
      name: @robot.name
      botroom: data.botroom || false
      username: data.username if data.botroom
    @receive new EnterMessage user, null

  onDeleteRoom: (data) =>
    # Delete any room that robot already left

    # find the room
    for r in [0...@rooms.length]
      @robot.logger.debug "room ke #{r} adalah #{@rooms[r].name}"
      if @rooms[r].room.room_id? and @rooms[r].room.room_id == data.room_id
        @robot.logger.debug "room to delete #{@rooms[r].name}"
        @rooms.splice r, 1
        @.saveRooms()

  onPostComment: (data) =>
    @robot.logger.debug "onPostComment, #{JSON.stringify data}"
    user = @robot.brain.userForId data.username_real,
      room: data.topic_id
      name: data.username
    @robot.logger.debug "User posting comment => #{user}"
    unless user.name is @robot.name
      msg = new TextMessage user, data.comment, data.comment_id
      @receive msg

exports.use = (robot) ->
  new Qiscus robot
